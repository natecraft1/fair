import Foundation
import PromiseKit
@testable import Fair

final class MockLocationManager: LocationManagerProtocol {
    func getCoordinates(for address: String) -> Promise<Coordinate> {
        let coord: Coordinate = Coordinate(latitude: 33.448, longitude: -112.07)
        return Promise(value: coord)
    }
    
    func clear() {
        distanceCache = [:]
    }
}
