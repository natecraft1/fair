import Foundation
import PromiseKit
@testable import Fair

class MockHttpService: HttpService {
    typealias Element = [Provider]
    var resolve: ((Element) -> Void)?
    var reject: ((Error) -> Void)?
    var didCallLoad = false
    
    func load<Element>(_ resource: Resource<Element>) -> Promise<Element> {
        didCallLoad = true
        let promise: Promise<Element> = Promise { resolve, reject in
            self.resolve = resolve as? ((MockHttpService.Element) -> Void)
            self.reject = reject
        }
        return promise
    }
    
    func flush() {
        self.resolve?(Provider.createMocks(1))
    }
}
