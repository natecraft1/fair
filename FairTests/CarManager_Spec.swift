
import Quick
import Nimble
@testable import Fair

class CarManager_Spec: QuickSpec {
    
    override func spec() {
        
        describe("CarManager") {
            
            var carManager: CarManager!
            var service: MockHttpService!
            var locationManager: MockLocationManager!
            
            beforeEach {
                carManager = CarManager()
                service = MockHttpService()
                locationManager = MockLocationManager()
                carManager.httpService = AnyHttpService<[Provider]>(service)
                carManager.locationManager = locationManager
            }
            
            describe("when a car manager is instantiated") {
                
                it("should contain an empty sections array") {
                    expect(carManager.isEmpty) == true
                }
                
                it("should set isFetching to false") {
                    expect(carManager.isFetching) == false
                }
                
                it("should have the default query params") {
                    expect(carManager.location.params) == CarQueryParams.defaultParams
                }
            }
            
            describe("fetch") {
                
                beforeEach {
                    _ = carManager.fetch(with: CarQueryParams.defaultParams)
                }
                
                it("should set isFetching to true") {
                    expect(carManager.isFetching) == true
                }
                
                it("should populate the model after completing the fetch") {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.7, execute: {
                        service.flush()
                        
                        expect(service.didCallLoad) == true
                        expect(carManager.isEmpty).toEventually(equal(false))
                        expect(carManager.isFetching).toEventually(equal(false))
                        expect(carManager.location.providers.count).to(beGreaterThan(0))
                    })
                }
            }
 
            describe("sort by") {
            
                var cars: [Car]!
                
                beforeEach {
                    let searchLocation = SearchLocation(params: CarQueryParams.defaultParams)
                    searchLocation.providers = Provider.createMocks(5)
                    searchLocation.coordinate = Coordinate(latitude: 34.02, longitude: -118.49)
                    carManager.location = searchLocation
                    carManager.locationManager.clear()
                }
                
                it("should be sorted by company and contain more than 1 section") {
                    expect(carManager.sortBy) == CarManager.SortBy.company
                    expect(carManager.sortBy.isMultiSection) == true
                    expect(carManager.sections.count).to(beGreaterThan(1))
                }
                
                it("should have cars sorted by price, ascending") {
                    carManager.sortBy = .priceAsc
                    cars = carManager.items
                    let firstCarPrice = Double(cars.first!.estimatedTotal.rate)!
                    let secondCarPrice = Double(cars[1].estimatedTotal.rate)!
                    let lastCarPrice = Double(cars.last!.estimatedTotal.rate)!

                    expect(carManager.sortBy.isMultiSection) == false
                    expect(carManager.sections.count) == 1

                    let isSortedByPriceAsc = firstCarPrice < secondCarPrice && secondCarPrice < lastCarPrice
                    expect(isSortedByPriceAsc) == true
                }
                
                it("should have cars sorted by price, descending") {
                    carManager.sortBy = .priceDesc
                    cars = carManager.items
                    let firstCarPrice = Double(cars.first!.estimatedTotal.rate)!
                    let secondCarPrice = Double(cars[1].estimatedTotal.rate)!
                    let lastCarPrice = Double(cars.last!.estimatedTotal.rate)!
                    
                    expect(carManager.sortBy.isMultiSection) == false
                    expect(carManager.sections.count) == 1
                    
                    let isSortedByPriceDesc = firstCarPrice > secondCarPrice && secondCarPrice > lastCarPrice
                    expect(isSortedByPriceDesc) == true
                }
                
                it("should have cars sorted by distance, ascending") {
                    carManager.sortBy = .distAsc
                    expect(carManager.sortBy.isMultiSection) == true
                    
                    let currentLoc = carManager.currentSearchCoordinate!
                    let d1 = currentLoc.getDistanceInMiles(to: carManager.sortedProviders.first!.location)
                    let d2 = currentLoc.getDistanceInMiles(to: carManager.sortedProviders[1].location)
                    let dl = currentLoc.getDistanceInMiles(to: carManager.sortedProviders.last!.location)
                    let isSortedByDistAsc = d1 < d2 && d2 < dl
                    expect(isSortedByDistAsc) == true
                }
                
                it("should have cars sorted by distance, descending") {
                    carManager.sortBy = .distDesc
                    expect(carManager.sortBy.isMultiSection) == true
                    
                    let currentLoc = carManager.currentSearchCoordinate!
                    let d1 = currentLoc.getDistanceInMiles(to: carManager.sortedProviders.first!.location)
                    let d2 = currentLoc.getDistanceInMiles(to: carManager.sortedProviders[1].location)
                    let dl = currentLoc.getDistanceInMiles(to: carManager.sortedProviders.last!.location)
                    let isSortedByDistDesc = d1 > d2 && d2 > dl
                    expect(isSortedByDistDesc) == true
                }
            }
        }
    }
}
