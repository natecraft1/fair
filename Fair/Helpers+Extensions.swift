import Foundation
import UIKit
import CoreLocation

extension Date {
    func toFormattedString(with format: String = "YYYY-MM-dd") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func constrain(height: CGFloat) {
        let heightConstraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: height)
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([heightConstraint])
    }
}

extension String {
    func toDouble() -> Double? {
        return Double(self)
    }
    
    func toRoundedInt() -> Int? {
        guard let double = self.toDouble()?.rounded() else { return nil }
        return Int(double)
    }
}

extension UILabel {
    func appendText(_ newText: String) {
        if let currentText = text {
            text = currentText + " " + newText
            return
        }
        text = newText
    }
}

extension CLLocationDistance {
    func miles() -> Double {
        let metersPerMile = 1609.344
        return self / metersPerMile
    }
}

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}


