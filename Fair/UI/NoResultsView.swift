//
//  NoResultsView.swift
//  Fair
//
//  Created by Nathan Glass on 3/28/18.
//  Copyright © 2018 Nathan Glass. All rights reserved.
//

import UIKit

class NoResultsView: UIView {

    static let height: CGFloat = 200
    
    @IBOutlet weak var noResultsLabel: UILabel!
    
}
