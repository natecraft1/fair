import UIKit
import Persei
import PromiseKit

class CarTableViewController: UITableViewController, ResultsManagerDelegate, MenuViewDelegate, UITextFieldDelegate, SearchHeaderDelegate {

    let carManager = CarManager()
    var indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
    let menu = MenuView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = carManager.fetch(with: CarQueryParams.defaultParams)
        carManager.delegate = self
        
        addActivityIndicator()
        setupMenu()
        indicator.startAnimating()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        guard !carManager.isEmpty else {
            displayNoResultsBackground()
            return 1
        }
        
        tableView.backgroundView = nil
        guard !carManager.isFetching else { return 0 }
        return carManager.sections.count
    }

    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return estimatedHeightForHeader(in: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return estimatedHeightForHeader(in: section)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return createHeader(for: section)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard !carManager.isFetching, !carManager.isEmpty else { return 0 }
        return carManager.sections[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath) as? CarTableViewCell else {
            fatalError("expected carTableViewCell")
        }
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor(red: 240/255, green: 240/255, blue: 250/255, alpha: 1.0) : .white
        cell.model = carManager.sections[indexPath.section][indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CarTableViewCell.height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Car", bundle: nil)
        guard let carDetailController = storyboard.instantiateViewController(withIdentifier: "carDetail") as? CarDetailViewController else { fatalError("expected car detail vc") }
        _ = carDetailController.view
        // ^ force the view to load
        carDetailController.model = carManager.sections[indexPath.section][indexPath.row]
        carDetailController.destination = carManager.sortedProviders[indexPath.section].location
        
        if let addr = carManager.address {
            let backgroundQueue = DispatchQueue.global(qos: .background)
            carManager.locationManager.getCoordinates(for: addr).then(on: backgroundQueue, execute: carDetailController.prepareMap)
        }
        self.navigationController?.pushViewController(carDetailController, animated: true)
    }
    
    func didCompleteFetch() {
        tableView.reloadData()
        indicator.stopAnimating()

        if !menu.revealed && !carManager.isEmpty {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                self?.menu.setRevealed(true, animated: true)
            }
        }
    }

    fileprivate func displayNoResultsBackground() {
        let noResultsView: NoResultsView = .fromNib()
        var text = carManager.isFetching ? "Fetching" : "No results found"
        if let address = carManager.address {
            text += " for \(address)"
        }
        noResultsView.noResultsLabel.text = text
        tableView.backgroundView = noResultsView
    }
    
    fileprivate func addActivityIndicator() {
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        indicator.hidesWhenStopped = true
        self.view.addSubview(indicator)
    }
    
    fileprivate func createHeader(for section: Int) -> UIView {
        
        let carHeaderView = createCarHeaderView(for: section)
        guard section == 0 else {
            return carHeaderView
        }
        
        let searchHeader = createSearchHeader()
        guard !carManager.isEmpty else {
            return searchHeader
        }
        
        carHeaderView.constrain(height: CarHeaderView.height)
        return createHeaderStackView(with: [searchHeader, carHeaderView])
    }
    
    fileprivate func createHeaderStackView(with views: [UIView]) -> UIView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        views.forEach(stackView.addArrangedSubview)
        return stackView
    }
    
    fileprivate func createSearchHeader() -> UIView {
        let searchHeader: SearchHeader = .fromNib()
        searchHeader.delegate = self
        if !searchHeader.isPopulated {
            searchHeader.populateDefaultQueryParams(carManager.location.params)
        }
        return searchHeader
    }

    fileprivate func createCarHeaderView(for section: Int) -> UIView {
        let headerView: CarHeaderView = .fromNib()
        
        if carManager.sortBy.isMultiSection {
            if let coord = carManager.currentSearchCoordinate {
                headerView.configure(provider: carManager.sortedProviders[section], searchLocation: coord)
            }
        } else {
            headerView.primaryLabel.text = "All cars"
            headerView.secondaryLabel.text = carManager.sortBy.sortedByText
        }
        
        return headerView
    }
    
    fileprivate func setupMenu() {
        tableView.addSubview(menu)
        menu.delegate = self
        menu.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        menu.items = CarManager.SortBy.all.compactMap { $0.icon }.map { createMenuItem(with: $0) }
    }
    
    fileprivate func createMenuItem(with icon: UIImage) -> MenuItem {
        var menuItem = MenuItem(image: icon)
        menuItem.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
        menuItem.highlightedBackgroundColor = .lightGray
        return menuItem
    }
    
    fileprivate func estimatedHeightForHeader(in section: Int) -> CGFloat {
        guard !carManager.isEmpty else { return SearchHeader.height }
        
        var height = CarHeaderView.height
        if section == 0 {
            height += SearchHeader.height
        }
        return height
    }
    
    func menu(_ menu: MenuView, didSelectItemAt index: Int) {
        carManager.sortBy = CarManager.SortBy.all[index]
        tableView.reloadData()
    }
    
    // MARK: Search header delegates
    func searchHeaderAllRequiredFieldsPopulated(queryParams: CarQueryParams) {
        guard !carManager.isFetching else { return }
        guard carManager.location.params != queryParams else { return }
        // triggers new fetch
        carManager.fetch(with: queryParams).always { [weak self] in
            guard let this = self else { return }
            this.tableView.alpha = 1
        }.catch { [weak self] (err) in
            guard let this = self else { return }
            // for now, assume no results
            this.indicator.stopAnimating()
            this.tableView.reloadData()
        }
        indicator.startAnimating()
        tableView.alpha = 0.8
    }
}
