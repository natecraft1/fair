import UIKit
import MapKit

class CarDetailViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var acLabel: UILabel!
    @IBOutlet weak var transmissionLabel: UILabel!
    @IBOutlet weak var estimatedTotalLabel: UILabel!
    @IBOutlet weak var ratesLabel: UILabel!
    @IBOutlet weak var carInfoStackView: UIStackView!
    var destination: Coordinate!
    
    var model: Car! {
        didSet {
            updateView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    func prepareMap(with sourceCoordinate: Coordinate) {
        mapView.delegate = self
        mapView.showsScale = true
        mapView.showsPointsOfInterest = true
        
        let sourceCoord = CLLocationCoordinate2D(latitude: sourceCoordinate.latitude, longitude: sourceCoordinate.longitude)
        let destCoord = CLLocationCoordinate2D(latitude: destination.latitude, longitude: destination.longitude)
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceCoord)
        let destinationPlacemark = MKPlacemark(coordinate: destCoord)
        
        let sourceItem = MKMapItem(placemark: sourcePlacemark)
        let destinationItem = MKMapItem(placemark: destinationPlacemark)
        
        let directionReq = MKDirectionsRequest()
        directionReq.source = sourceItem
        directionReq.destination = destinationItem
        directionReq.transportType = .automobile
        
        let directions = MKDirections(request: directionReq)
        
        directions.calculate { [weak self] (res, err) in
            guard err == nil else { print(err ?? ""); return }
            guard let route = res?.routes.first, let this = self else { return }
            
            DispatchQueue.main.async {
                route.steps.forEach({ (step) in
                    let directionLabel = InsetLabel()
                    directionLabel.text = String(step.distance.miles().roundToDecimal(2)) + " mi  " + step.instructions
                    this.carInfoStackView.addArrangedSubview(directionLabel)
                })
                this.mapView.add(route.polyline, level: .aboveRoads)
                let rect = route.polyline.boundingMapRect
                let region = MKCoordinateRegionForMapRect(rect)
                this.mapView.setRegion(region, animated: true)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
    fileprivate func updateView() {
        typeLabel.appendText(model.type)
        categoryLabel.appendText(model.category)
        acLabel.appendText(model.airConditioned ? "Yes" : "No")
        transmissionLabel.appendText(model.transmission)
        estimatedTotalLabel.appendText(model.estimatedTotal.currency + " " + model.estimatedTotal.rate)
        let rates = model.rates.map { String($0.price.rate) + "/" + $0.type }
        ratesLabel.appendText(rates.joined(separator: ", ").lowercased())
    }
    
}
