//
//  CarDetailInfoLabel.swift
//  Fair
//
//  Created by Nathan Glass on 3/28/18.
//  Copyright © 2018 Nathan Glass. All rights reserved.
//

import UIKit

class InsetLabel: UILabel {

    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: 0, left: 14, bottom: 0, right: 5)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

}
