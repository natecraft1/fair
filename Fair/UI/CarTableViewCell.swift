import UIKit

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    static let height: CGFloat = 60
    
    var model: Car! {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    fileprivate func updateView() {
        categoryLabel.text = model.category + ", " + model.type
        guard let price = model.estimatedTotal.rate.toRoundedInt() else { return }
        priceLabel.text = model.estimatedTotal.currency + " " + String(price)
    }
}
