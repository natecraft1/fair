import UIKit
import DateTimePicker

protocol SearchHeaderDelegate: class {
    func searchHeaderAllRequiredFieldsPopulated(queryParams: CarQueryParams)
}

class SearchHeader: UIView {

    static let height: CGFloat = 105
    @IBOutlet weak var pickupAddressTextField: UITextField!
    @IBOutlet weak var pickupCalendarButton: UIButton!
    @IBOutlet weak var dropoffCalendarButton: UIButton!
    @IBOutlet weak var pickupDateLabel: UILabel!
    @IBOutlet weak var dropoffDateLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    weak var delegate: SearchHeaderDelegate?
    var isPopulated: Bool = false
    let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: Date())
    
    var pickupDate: Date? {
        didSet {
            pickupDateLabel.text = pickupDate?.toFormattedString()
        }
    }
    var dropoffDate: Date? {
        didSet {
            dropoffDateLabel.text = dropoffDate?.toFormattedString()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchButton.layer.cornerRadius = 6.0
    }
    
    @IBAction func pickupCalendarButtonPressed(_ sender: UIButton) {
        let picker = DateTimePicker.show(selected: nil, minimumDate: tomorrow, maximumDate: dropoffDate, timeInterval: .default)
        picker.isDatePickerOnly = true
        picker.doneBackgroundColor = UIColor.darkGray
        picker.completionHandler = { [weak self] date in
            self?.pickupDate = date
            self?.notifyNewQueryParams()
        }
    }
    
    @IBAction func dropoffCalendarButtonPressed(_ sender: UIButton) {
        let picker = DateTimePicker.show(selected: nil, minimumDate: pickupDate ?? tomorrow, maximumDate: nil, timeInterval: .default)
        picker.isDatePickerOnly = true
        picker.doneBackgroundColor = UIColor.darkGray
        picker.completionHandler = { [weak self] date in
            self?.dropoffDate = date
            self?.notifyNewQueryParams()
        }
    }
    
    func notifyNewQueryParams() {
        guard let addressText = pickupAddressTextField.text, addressText.isEmpty == false
            , let pickupDate = pickupDate
            , let dropoffDate = dropoffDate else { return }
        
        delegate?.searchHeaderAllRequiredFieldsPopulated(queryParams: CarQueryParams(pickup: pickupDate, dropoff: dropoffDate, address: addressText))
    }
    
    func populateDefaultQueryParams(_ params: CarQueryParams) {
        isPopulated = true
        pickupDate = params.pickup
        dropoffDate = params.dropoff
        pickupAddressTextField.text = params.address
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        pickupAddressTextField.resignFirstResponder()
        notifyNewQueryParams()
    }
    
    deinit {
        print("deallocating")
    }
}
