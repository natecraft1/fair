import UIKit

class CarHeaderView: UIView {
    
    static let height: CGFloat = 62
    
    @IBOutlet weak var primaryLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    func configure(provider: Provider, searchLocation: Coordinate) {
        primaryLabel.text = provider.name
        secondaryLabel.text = provider.address
        let distance = searchLocation.getDistanceInMiles(to: provider.location)
        distanceLabel.text = "\(distance.roundToDecimal(2)) mi"
    }

}
