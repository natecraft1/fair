import SwiftyJSON

struct Rate {
    var type: String
    var price: Price
}

extension Rate {
    init?(data: JSON) {
        guard let type = data["type"].string
            , let price = Price(data: data["price"]) else { return nil }
        
        self.type = type
        self.price = price
    }
}
