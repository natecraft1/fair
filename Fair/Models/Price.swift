import SwiftyJSON

struct Price {
    var currency: String
    var rate: String
}

extension Price {
    init?(data: JSON) {
        guard let currency = data["currency"].string
            , let rate = data["amount"].string else { return nil }
        
        self.currency = currency
        self.rate = rate
    }
}

