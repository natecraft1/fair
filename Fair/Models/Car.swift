import SwiftyJSON

struct Car {
    var transmission: String
    var rates: [Rate]
    var estimatedTotal: Price
    var airConditioned: Bool
    var category: String
    var type: String
}

extension Car {
    init?(data: JSON) {
        let info = data["vehicle_info"]
        guard let transmission = info["transmission"].string
            , let rates = data["rates"].array
            , let estimatedTotal = Price(data: data["estimated_total"]) else {
                return nil
                
        }
        
        self.transmission = transmission
        self.rates = rates.compactMap(Rate.init)
        self.estimatedTotal = estimatedTotal
        self.airConditioned = info["air_conditioning"].bool ?? true
        self.category = info["category"].string ?? ""
        self.type = info["type"].string ?? ""
    }
}

extension Car {
    
    static func createMocks(_ number: Int, startingPrice: Int = 50) -> [Car] {
        return (0..<number).map { num -> Car in
            return Car(transmission: "", rates: [], estimatedTotal: Price(currency: "USD", rate: "\(startingPrice + num)"), airConditioned: true, category: "", type: "")
        }
    }

}
