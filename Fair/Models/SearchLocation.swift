import Foundation

final class SearchLocation {
    var params: CarQueryParams = CarQueryParams.defaultParams
    var providers: [Provider] = []
    var coordinate: Coordinate?
    
    init(params: CarQueryParams = CarQueryParams.defaultParams) {
        self.params = params
    }
}
