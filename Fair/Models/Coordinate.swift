import Foundation
import CoreLocation

public struct Coordinate {
    var latitude: Double
    var longitude: Double
}

extension Coordinate: Hashable {
    public var hashValue: Int {
        return latitude.hashValue ^ longitude.hashValue &* 16777619
    }
    
    public static func == (lhs: Coordinate, rhs: Coordinate) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}

extension Coordinate {
    func getDistanceInMiles(to: Coordinate) -> Double {
        if let distance = distanceCache[to] { return distance }
        
        let locationA = CLLocation(latitude: latitude, longitude: longitude)
        let locationB = CLLocation(latitude: to.latitude, longitude: to.longitude)
        let distInMeters = locationA.distance(from: locationB)
        let miles = distInMeters.miles()
        distanceCache[to] = miles
        return miles
    }
}
