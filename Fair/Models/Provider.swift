import SwiftyJSON

struct Provider {
    var name: String
    var location: Coordinate
    var address: String
    var cars: [Car]
}

extension Provider {
    init?(data: JSON) {
        guard let name = data["provider"]["company_name"].string
            , let latitude = data["location"]["latitude"].double
            , let longitude = data["location"]["longitude"].double
            , let addressDict = data["address"].dictionary
            , let cars = data["cars"].array else { return nil }
        
        self.name = name
        self.location = Coordinate(latitude: latitude, longitude: longitude)
        let addr = Array(addressDict.mapValues { $0.string }.values)
        self.address = addr.compactMap { $0 }.joined(separator: ", ")
        self.cars = cars.compactMap(Car.init)
    }
    
    static let parse: (Data) throws -> [Provider] = { data in
        do {
            let json = try JSON(data: data)
            guard let resultsArray = json["results"].array else { throw AmadeusDataError.jsonParsing }
            return resultsArray.compactMap(Provider.init)
        } catch {
            throw AmadeusDataError.jsonParsing
        }
    }
}

extension Provider {
    static func createMocks(_ number: Int) -> [Provider] {
        return (0..<number).map { num -> Provider in
            let coordinate = Coordinate(latitude: 34.02 - Double(num)/100, longitude: -118.49)
            return Provider(name: "test\(num)", location: coordinate, address: "test street", cars: Car.createMocks(3, startingPrice: num * 20))
        }
    }
}

