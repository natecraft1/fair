import Foundation
import PromiseKit
import SwiftyJSON

enum AmadeusDataError: Error {
    case locationNotFound
    case deallocated
    case invalidUrl
    case jsonParsing
}

protocol ResultsManager: class {
    associatedtype DataType
    var sections: [[DataType]] { get }
    var isFetching: Bool { get }
    var isEmpty: Bool { get }
    var delegate: ResultsManagerDelegate? { get set }
}

protocol ResultsManagerDelegate: class {
    func didCompleteFetch()
}

struct CarQueryParams: Equatable {
    var pickup: Date
    var dropoff: Date
    var address: String
}

extension CarQueryParams {
    static let defaultParams = CarQueryParams(pickup: Calendar.current.date(byAdding: .day, value: 1, to: Date())!, dropoff: Calendar.current.date(byAdding: .day, value: 3, to: Date())!, address: "Santa Monica, CA")
}

func ==(lhs: CarQueryParams, rhs: CarQueryParams) -> Bool {
    return lhs.pickup == rhs.pickup
        && lhs.dropoff == rhs.dropoff
        && lhs.address == rhs.address
}

class CarManager: ResultsManager {
    
    // MARK: ResultsManager delegates
    typealias DataType = Car

    var isFetching: Bool = false
    var location = SearchLocation()
    // MARK: CarManager specific properties
    var httpService = AnyHttpService(HttpServiceManager<[Provider]>())
    var locationManager: LocationManagerProtocol = LocationManager()
    weak var delegate: ResultsManagerDelegate?
    
    fileprivate var sortedSections: [[DataType]]?
    fileprivate var sortedProvidersCache: [Provider]?

    var providers: [Provider] {
        return location.providers
    }
    
    var sections: [[DataType]] {
        guard !isFetching else { return [[]] }
        if let cached = sortedSections {
            return cached
        }
        let sorted = sortBy.sortToSections(location)
        sortedSections = sorted
        return sorted
    }

    var items: [DataType] {
        return sections.flatMap { $0 } 
    }

    var isEmpty: Bool {
        return sections.flatMap { $0 }.isEmpty
    }
    
    var address: String? {
        return location.params.address
    }

    var currentSearchCoordinate: Coordinate? {
        return location.coordinate
    }
    
    var sortBy: SortBy = .company {
        didSet {
            sortedSections = nil
            sortedProvidersCache = nil
        }
    }
    
    var sortedProviders: [Provider] {
        if let cachedResults = sortedProvidersCache {
            return cachedResults
        }

        var results: [Provider]
        switch sortBy {
        case .company, .priceAsc, .priceDesc:
            results = providers
        case .distAsc:
            results = sortBy.sortByDistance(location, ascending: true)
        case .distDesc:
            results = sortBy.sortByDistance(location, ascending: false)
        }
        sortedProvidersCache = results

        return results
    }
    
    func fetch(with queryParams: CarQueryParams) -> Promise<Void> {
        reset()
        location = SearchLocation(params: queryParams)
        // populate the coordinate cache
        _ = getCoordinatesForCurrentAddress()
        isFetching = true
        
        return buildQueryUrl(queryParams: queryParams)
            .then { [weak self] (url) -> Promise<([Provider], Coordinate)> in
                guard let this = self else { throw AmadeusDataError.deallocated }
                let providersResource = Resource(url: url, parse: Provider.parse)
                let providerPromise = this.httpService.load(providersResource)
                let currentLocationPromise = this.locationManager.getCoordinates(for: queryParams.address)
                return when(fulfilled: providerPromise, currentLocationPromise)
            }.then(on: .main) { [weak self] (providers, currentLocation) -> Void in
                guard let this = self else { return }
                this.isFetching = false
                this.location.providers = providers
                this.location.coordinate = currentLocation
            }.always { [weak self] in
                guard let this = self else { return }
                this.isFetching = false
                this.delegate?.didCompleteFetch()
        }
    }
    
    func getCoordinatesForCurrentAddress() -> Promise<Coordinate>? {
        guard let address = self.address else { return nil }
        return locationManager.getCoordinates(for: address)
    }
    
    fileprivate func buildQueryUrl(queryParams: CarQueryParams) -> Promise<URL> {
        return locationManager.getCoordinates(for: queryParams.address).then { (coordinates) -> URL in
            guard let url = AmadeusUrlBuilder.buildUrl(coordinates: coordinates, pickup: queryParams.pickup, dropoff: queryParams.dropoff) else {
                throw AmadeusDataError.invalidUrl
            }
            return url
        }
    }
    
    fileprivate func reset() {
        sortedSections = nil
        sortedProvidersCache = nil
        location = SearchLocation()
        locationManager.clear()
    }
    
}

extension CarManager {
    
    enum SortBy {
        case company
        case priceAsc
        case priceDesc
        case distAsc
        case distDesc
        
        var sortedByText: String {
            switch self {
            case .company:
                return "Company"
            case .priceAsc:
                return "Price (Low to High)"
            case .priceDesc:
                return "Price (High to Low)"
            case .distAsc:
                return "Distance (Closest)"
            case .distDesc:
                return "Distance (Furthest)"
            }
        }
        
        var isMultiSection: Bool {
            switch self {
            case .company, .distAsc, .distDesc:
                return true
            default:
                return false
            }
        }
        
        func sortToSections(_ location: SearchLocation) -> [[DataType]] {
            switch self {
            case .company:
                return location.providers.map { $0.cars }
            case .priceAsc:
                return sortByPrice(location, ascending: true)
            case .priceDesc:
                return sortByPrice(location, ascending: false)
            case .distAsc:
                return sortByDistanceToSections(location, ascending: true)
            case .distDesc:
                return sortByDistanceToSections(location, ascending: false)
            }
        }
        
        fileprivate func sortByDistance(_ location: SearchLocation, ascending: Bool) -> [Provider] {
            guard let currentCoorinate = location.coordinate else { return location.providers }
            return location.providers.sorted(by: { (s1, s2) -> Bool in
                let d1 = currentCoorinate.getDistanceInMiles(to: s1.location)
                let d2 = currentCoorinate.getDistanceInMiles(to: s2.location)
                return ascending ? d1 < d2 : d1 > d2
            })
        }
        
        fileprivate func sortByDistanceToSections(_ location: SearchLocation, ascending: Bool) -> [[DataType]] {
            let sorted = sortByDistance(location, ascending: ascending)
            return sorted.map { $0.cars }
        }
        
        fileprivate func sortByPrice(_ location: SearchLocation, ascending: Bool) -> [[DataType]] {
            let items = location.providers.flatMap { $0.cars }
            let sortedItems = items.sorted(by: { (item1, item2) -> Bool in
                guard let rate1 = item1.estimatedTotal.rate.toDouble()
                    , let rate2 = item2.estimatedTotal.rate.toDouble() else { return true }
                return ascending ? rate1 < rate2 : rate1 > rate2
            })
            return [sortedItems]
        }
        
        var icon: UIImage {
            // normally I would never force unwrap these
            switch self {
            case .company:
                return UIImage(named: "company")!
            case .distAsc:
                return UIImage(named: "distance-close")!
            case .distDesc:
                return UIImage(named: "distance-far")!
            case .priceAsc:
                return UIImage(named: "dollar")!
            case .priceDesc:
                return UIImage(named: "dollar-red")!
            }
        }
        
        static let all: [SortBy] = [.company, .priceAsc, .priceDesc, .distAsc, .distDesc]
    }
}
