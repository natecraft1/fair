import Foundation
import PromiseKit
import CoreLocation

public var distanceCache: [Coordinate: Double] = [:]

protocol LocationManagerProtocol: class {
    func getCoordinates(for address: String) -> Promise<Coordinate>
    func clear()
}

final class LocationManager: LocationManagerProtocol {
    
    fileprivate var coordinateCache: [String: Coordinate] = [:]
    
    func getCoordinates(for address: String) -> Promise<Coordinate> {
        
        if let coords = coordinateCache[address] {
            return Promise(value: coords)
        }
        
        let geoCoder = CLGeocoder()
        let geoPromise: Promise<Coordinate> = Promise { resolve, reject in
            geoCoder.geocodeAddressString(address) { [weak self] (placemarks, error) in
                if let error = error { reject(error); return }
                guard let placemarks = placemarks
                    , let location = placemarks.first?.location else {
                        reject(AmadeusDataError.locationNotFound)
                        return
                }
                let latLon = Coordinate(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                self?.coordinateCache[address] = latLon
                resolve(latLon)
            }
        }
        return geoPromise
    }
    
    func clear() {
        distanceCache = [:]
    }
}
