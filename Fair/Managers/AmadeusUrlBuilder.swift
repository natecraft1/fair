import Foundation

final class AmadeusUrlBuilder {
    static let baseUrl = "https://api.sandbox.amadeus.com/v1.2/cars/search-circle"
    static let apiKey = "kFz8Rr1AmhkYb7uc0oTxjTBl5xRuDLha"

    static func buildUrl(coordinates: Coordinate, pickup: Date, dropoff: Date) -> URL? {
        let formattedPickup = pickup.toFormattedString()
        let formattedDropoff = dropoff.toFormattedString()
        
        let urlString = "\(baseUrl)?apikey=\(apiKey)&latitude=\(coordinates.latitude)&longitude=\(coordinates.longitude)&pick_up=\(formattedPickup)&drop_off=\(formattedDropoff)"
        
        return URL(string: urlString)
    }

}
