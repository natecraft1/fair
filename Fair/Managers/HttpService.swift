import Foundation
import PromiseKit
import SwiftyJSON

enum HttpError: Error {
    case dataNotFound
}

struct Resource<T> {
    var url: URL
    var parse: (Data) throws -> T
}

protocol HttpService {
    associatedtype Element
    func load<Element>(_ resource: Resource<Element>) -> Promise<Element>
}

final class HttpServiceManager<T>: HttpService {
    typealias Element = T
    func load<Element>(_ resource: Resource<Element>) -> Promise<Element> {
        let promise: Promise<Element> = Promise { resolve, reject in
            URLSession.shared.dataTask(with: resource.url) { (data, _, err) in
                if let error = err { reject(error); return }
                guard let data = data else { reject(HttpError.dataNotFound); return }
                if let parsedData = try? resource.parse(data) {
                    resolve(parsedData)
                } else {
                    reject(AmadeusDataError.jsonParsing)
                }
            }.resume()
        }
        return promise
    }
}

struct AnyHttpService<Element>: HttpService {

    private let boxService: _AnyHttpServiceBase<Element>
    
    init<S: HttpService>(_ service: S) where S.Element == Element {
        self.boxService = _AnyHttpServiceBox(service)
    }
    
    func load<Element>(_ resource: Resource<Element>) -> Promise<Element> {
        return boxService.load(resource)
    }
}

private class _AnyHttpServiceBase<Element>: HttpService {
    
    init() {
        guard type(of: self) != _AnyHttpServiceBase.self else {
            fatalError("must create from subclass")
        }
    }
    
    func load<Element>(_ resource: Resource<Element>) -> Promise<Element> {
        fatalError("must override")
    }
}

private final class _AnyHttpServiceBox<S: HttpService>: _AnyHttpServiceBase<S.Element> {
    var service: S
    init(_ service: S) {
        self.service = service
    }
    
    override func load<Element>(_ resource: Resource<Element>) -> Promise<Element> {
        return service.load(resource)
    }
}
